#ifndef GOOPENZWAVE_MANAGER
#define GOOPENZWAVE_MANAGER

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "zwbytes.h"
#include "zwlist.h"
#include "gzw_options.h"
#include "gzw_notification.h"

// Exception handling
struct StrResult {
  char* val;
  const char *error;
};

struct VoidResult {
  const char *error;
};

struct BoolResult {
  bool val;
  const char *error;
};

struct Uint32Result {
  uint32_t val;
  const char *error;
};

struct int32Result {
  int32_t val;
  const char *error;
};

struct Uint16Result {
  uint16_t val;
  const char *error;
};

struct Uint8Result {
  uint8_t val;
  const char *error;
};


#ifdef __cplusplus
extern "C" {
#endif

	// Types.
	typedef void* manager_t;

	//
	// Construction.
	//

	manager_t manager_create();
	manager_t manager_get();
	void manager_destroy();
	char* manager_getVersionAsString(); /*!< C string must be freed. */
	char* manager_getVersionLongAsString(); /*!< C string must be freed. */
	void manager_getVersion(uint16_t *major, uint16_t *minor);

	//
	// Configuration.
	//

	options_t manager_getOptions(manager_t m);

	//
	// Drivers.
	//

	bool manager_addDriver(manager_t m, const char* controllerPath);
	bool manager_removeDriver(manager_t m, const char* controllerPath);
	uint8_t manager_getControllerNodeId(manager_t m, uint32_t homeId);
	uint8_t manager_getSUCNodeId(manager_t m, uint32_t homeId);
	bool manager_isPrimaryController(manager_t m, uint32_t homeId);
	bool manager_isStaticUpdateController(manager_t m, uint32_t homeId);
	bool manager_isBridgeController(manager_t m, uint32_t homeId);
	char* manager_getLibraryVersion(manager_t m, uint32_t homeId);  /*!< C string must be freed. */
	char* manager_getLibraryTypeName(manager_t m, uint32_t homeId); /*!< C string must be freed. */
	int32_t manager_getSendQueueCount(manager_t m, uint32_t homeId);
	void manager_logDriverStatistics(manager_t m, uint32_t homeId);
//TODO driver_controllerInterface_t manager_getControllerInterfaceType(manager_t m, uint32_t homeId);
	char* manager_getControllerPath(manager_t m, uint32_t homeId);  /*!< C string must be freed. */

	//
	// Polling Z-Wave devices.
	//

	int32_t manager_getPollInterval(manager_t m);
	void manager_setPollInterval(manager_t m, int32_t milliseconds, bool intervalBetweenPolls);
	bool manager_enablePoll(manager_t m, valueid_t valueid, uint8_t intensity);
	bool manager_disablePoll(manager_t m, valueid_t valueid);
	bool manager_isPolled(manager_t m, valueid_t valueid);
	void manager_setPollIntensity(manager_t m, valueid_t valueid, uint8_t intensity);
	uint8_t manager_getPollIntensity(manager_t m, valueid_t valueid);

	//
	// Node information.
	//

	struct BoolResult manager_refreshNodeInfo(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_requestNodeState(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_requestNodeDynamic(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_isNodeListeningDevice(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_isNodeFrequentListeningDevice(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_isNodeBeamingDevice(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_isNodeRoutingDevice(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_isNodeSecurityDevice(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct Uint32Result manager_getNodeMaxBaudRate(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct Uint8Result manager_getNodeVersion(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct Uint8Result manager_getNodeSecurity(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_isNodeZWavePlus(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct Uint8Result manager_getNodeBasic(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct Uint8Result manager_getNodeGeneric(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct Uint8Result manager_getNodeSpecific(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct StrResult manager_getNodeType(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct StrResult manager_getNodeManufacturerName(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct StrResult manager_getNodeProductName(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct StrResult manager_getNodeName(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct StrResult manager_getNodeLocation(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct StrResult manager_getNodeManufacturerId(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct StrResult manager_getNodeProductType(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct StrResult manager_getNodeProductId(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct VoidResult manager_setNodeManufacturerName(manager_t m, uint32_t homeId, uint8_t nodeId, const char* manufacturerName);
	struct VoidResult manager_setNodeProductName(manager_t m, uint32_t homeId, uint8_t nodeId, const char* productName);
	struct VoidResult manager_setNodeName(manager_t m, uint32_t homeId, uint8_t nodeId, const char* nodeName);
	struct VoidResult manager_setNodeLocation(manager_t m, uint32_t homeId, uint8_t nodeId, const char* location);
	struct VoidResult manager_setNodeOn(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct VoidResult manager_setNodeOff(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct VoidResult manager_setNodeLevel(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t level);
	struct BoolResult manager_isNodeInfoReceived(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_getNodeClassInformation(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t commandClassId, char **o_name, uint8_t *o_version);
	struct BoolResult manager_isNodeAwake(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_isNodeFailed(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct StrResult manager_getNodeQueryStage(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct Uint16Result manager_getNodeDeviceType(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct StrResult manager_getNodeDeviceTypeString(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct Uint8Result manager_getNodeRole(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct StrResult manager_getNodeRoleString(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */
	struct Uint8Result manager_getNodePlusType(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct StrResult manager_getNodePlusTypeString(manager_t m, uint32_t homeId, uint8_t nodeId); /*!< C string must be freed. */

	//
	// Values.
	//

	struct StrResult manager_getValueLabel(manager_t m, valueid_t valueid);
	struct VoidResult manager_setValueLabel(manager_t m, valueid_t valueid, const char* value);
	struct StrResult manager_getValueUnits(manager_t m, valueid_t valueid);
	struct VoidResult manager_setValueUnits(manager_t m, valueid_t valueid, const char* value);
	struct StrResult manager_getValueHelp(manager_t m, valueid_t valueid);
	struct VoidResult manager_setValueHelp(manager_t m, valueid_t valueid, const char* value);
	struct int32Result manager_getValueMin(manager_t m, valueid_t valueid);
	struct int32Result manager_getValueMax(manager_t m, valueid_t valueid);
	struct BoolResult manager_isValueReadOnly(manager_t m, valueid_t valueid);
	struct BoolResult manager_isValueWriteOnly(manager_t m, valueid_t valueid);
	struct BoolResult manager_isValueSet(manager_t m, valueid_t valueid);
	struct BoolResult manager_isValuePolled(manager_t m, valueid_t valueid);
	struct BoolResult manager_getValueAsBool(manager_t m, valueid_t valueid, bool *o_value);
	struct BoolResult manager_getValueAsByte(manager_t m, valueid_t valueid, uint8_t *o_value);
	struct BoolResult manager_getValueAsFloat(manager_t m, valueid_t valueid, float *o_value);
	struct BoolResult manager_getValueAsInt(manager_t m, valueid_t valueid, int32_t *o_value);
	struct BoolResult manager_getValueAsShort(manager_t m, valueid_t valueid, int16_t *o_value);
	struct BoolResult manager_getValueAsString(manager_t m, valueid_t valueid, char **o_value);
	struct BoolResult manager_getValueAsRaw(manager_t m, valueid_t valueid, zwbytes_t *o_value);
	struct BoolResult manager_getValueListSelectionAsString(manager_t m, valueid_t valueid, char **o_value);
	struct BoolResult manager_getValueListSelectionAsInt32(manager_t m, valueid_t valueid, int32_t *o_value);
	struct BoolResult manager_getValueListItems(manager_t m, valueid_t valueid, zwlist_t **o_value);
	struct BoolResult manager_getValueFloatPrecision(manager_t m, valueid_t valueid, uint8_t *o_value);
	struct BoolResult manager_setValueBool(manager_t m, valueid_t valueid, bool value);
	struct BoolResult manager_setValueUint8(manager_t m, valueid_t valueid, uint8_t value);
	struct BoolResult manager_setValueFloat(manager_t m, valueid_t valueid, float value);
	struct BoolResult manager_setValueInt32(manager_t m, valueid_t valueid, int32_t value);
	struct BoolResult manager_setValueInt16(manager_t m, valueid_t valueid, int16_t value);
	struct BoolResult manager_setValueBytes(manager_t m, valueid_t valueid, zwbytes_t *value);
	struct BoolResult manager_setValueString(manager_t m, valueid_t valueid, const char* value);
	struct BoolResult manager_setValueListSelection(manager_t m, valueid_t valueid, const char* selectedItem);
	struct BoolResult manager_refreshValue(manager_t m, valueid_t valueid);
	struct VoidResult manager_setChangeVerified(manager_t m, valueid_t valueid, bool verify);
	struct BoolResult manager_getChangeVerified(manager_t m, valueid_t valueid);
	struct BoolResult manager_pressButton(manager_t m, valueid_t valueid);
	struct BoolResult manager_releaseButton(manager_t m, valueid_t valueid);

	//
	// Climate control schedules.
	//

	struct Uint8Result manager_getNumSwitchPoints(manager_t m, valueid_t valueid);
	struct BoolResult manager_setSwitchPoint(manager_t m, valueid_t valueid, uint8_t hours, uint8_t minutes, int8_t setback);
	struct BoolResult manager_removeSwitchPoint(manager_t m, valueid_t valueid, uint8_t hours, uint8_t minutes);
	struct VoidResult manager_clearSwitchPoints(manager_t m, valueid_t valueid);
	struct BoolResult manager_getSwitchPoint(manager_t m, valueid_t valueid, uint8_t idx, uint8_t *o_hours, uint8_t *o_minutes, int8_t *o_setback);

	//
	// Configuration parameters.
	//

	struct BoolResult manager_setConfigParam(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t param, int32_t value, uint8_t size);
	struct VoidResult manager_requestConfigParam(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t param);
	struct VoidResult manager_requestAllConfigParams(manager_t m, uint32_t homeId, uint8_t nodeId);

	//
	// Groups.
	//

	struct Uint8Result manager_getNumGroups(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct Uint32Result manager_getAssociations(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, uint8_t **o_associations);
//TODO uint32_t manager_getAssociations(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, instanceassociation_t **o_associations);
	struct Uint8Result manager_getMaxAssociations(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx);
	struct StrResult manager_getGroupLabel(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx);
	struct VoidResult manager_addAssociation(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, uint8_t targetNodeId, uint8_t instance);
	struct VoidResult manager_removeAssociation(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, uint8_t targetNodeId, uint8_t instance);

	//
	// Notifications.
	//

	extern void goNotificationCB(notification_t notification, void *userdata); /*!< Must be implemented in cgo. */
	bool manager_addWatcher(manager_t m, void *userdata);
	bool manager_removeWatcher(manager_t m, void *userdata);

	//
	// Controller commands.
	//

	void manager_resetController(manager_t m, uint32_t homeId);
	void manager_softReset(manager_t m, uint32_t homeId);
	bool manager_cancelControllerCommand(manager_t m, uint32_t homeId);

	//
	// Network commands.
	//

	struct VoidResult manager_testNetworkNode(manager_t m, uint32_t homeId, uint8_t nodeId, uint32_t count);
	struct VoidResult manager_testNetwork(manager_t m, uint32_t homeId, uint32_t count);
	struct VoidResult manager_healNetworkNode(manager_t m, uint32_t homeId, uint8_t nodeId, bool doRR);
	struct VoidResult manager_healNetwork(manager_t m, uint32_t homeId, bool doRR);
	struct BoolResult manager_addNode(manager_t m, uint32_t homeId, bool doSecurity);
	struct BoolResult manager_removeNode(manager_t m, uint32_t homeId);
	struct BoolResult manager_removeFailedNode(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_hasNodeFailed(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_requestNodeNeighborUpdate(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_assignReturnRoute(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_deleteAllReturnRoutes(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_sendNodeInformation(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_createNewPrimary(manager_t m, uint32_t homeId);
	struct BoolResult manager_receiveConfiguration(manager_t m, uint32_t homeId);
	struct BoolResult manager_replaceFailedNode(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_transferPrimaryRole(manager_t m, uint32_t homeId);
	struct BoolResult manager_requestNetworkUpdate(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_replicationSend(manager_t m, uint32_t homeId, uint8_t nodeId);
	struct BoolResult manager_createButton(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t buttonid);
	struct BoolResult manager_deleteButton(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t buttonid);


//TODO void manager_getDriverStatistics(manager_t m, uint32_t homeId, driver_driverdata_t *data);
//TODO void manager_getNodeStatistics(manager_t m, uint32_t homeId, uint8_t nodeId, node_nodedata_t *data);

#ifdef __cplusplus
}
#endif

#endif // define GOOPENZWAVE_MANAGER
