#include "gzw_manager.h"
#include <string.h>
#include <Manager.h>
#include <Notification.h>
#include <Defs.h>


template<typename Ret, typename Callable>
auto exception_boundary_handle(Callable&& func) -> Ret
{
    Ret result = {0};
    try {
        result.val = func();
    } catch( OpenZWave::OZWException &e ) {
        result.error = strdup(e.GetMsg().c_str());
    } catch( const std::exception &e ) {
        result.error = strdup(e.what());
    }
    return result;
}

template<typename Callable>
struct BoolResult exception_boundary_handle_bool(Callable&& func)
{
    struct BoolResult result = {0};
    try {
        result.val = func();
    } catch( OpenZWave::OZWException &e ) {
        result.error = strdup(e.GetMsg().c_str());
    } catch( const std::exception &e ) {
        result.error = strdup(e.what());
    }
    return result;
}

template<typename Callable>
auto exception_boundary_handle_void(Callable&& func) -> VoidResult
{
    VoidResult result = {0};
    try {
        func();
    } catch( OpenZWave::OZWException &e ) {
        result.error = strdup(e.GetMsg().c_str());
    } catch( const std::exception &e ) {
        result.error = strdup(e.what());
    }
    return result;
}

//
// Construction.
//

manager_t manager_create()
{
	OpenZWave::Manager *man = OpenZWave::Manager::Create();
	return (manager_t)man;
}

manager_t manager_get()
{
	OpenZWave::Manager *man = OpenZWave::Manager::Get();
	return (manager_t)man;
}

void manager_destroy()
{
	OpenZWave::Manager::Destroy();
}

char* manager_getVersionAsString()
{
	return strdup(OpenZWave::Manager::getVersionAsString().c_str());
}

char* manager_getVersionLongAsString()
{
	return strdup(OpenZWave::Manager::getVersionLongAsString().c_str());
}

void manager_getVersion(uint16_t *major, uint16_t *minor)
{
	ozwversion ver = OpenZWave::Manager::getVersion();
	*major = ver._v >> 16 & 0x00FF;
	*minor = ver._v & 0x00FF;
}

//
// Configuration.
//

options_t manager_getOptions(manager_t m)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return (options_t)man->GetOptions();
}

//
// Drivers.
//

bool manager_addDriver(manager_t m, const char *controllerPath)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	if (strcasecmp(controllerPath, "usb") == 0) {
		return man->AddDriver("HID Controller", OpenZWave::Driver::ControllerInterface_Hid);
	} else {
		return man->AddDriver(controllerPath);
	}
}

bool manager_removeDriver(manager_t m, const char *controllerPath)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	if (strcasecmp(controllerPath, "usb") == 0) {
		return man->RemoveDriver("HID Controller");
	} else {
		return man->RemoveDriver(controllerPath);
	}
}

uint8_t manager_getControllerNodeId(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->GetControllerNodeId(homeId);
}

uint8_t manager_getSUCNodeId(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->GetSUCNodeId(homeId);
}

bool manager_isPrimaryController(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->IsPrimaryController(homeId);
}

bool manager_isStaticUpdateController(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->IsStaticUpdateController(homeId);
}

bool manager_isBridgeController(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->IsBridgeController(homeId);
}

char* manager_getLibraryVersion(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return strdup(man->GetLibraryVersion(homeId).c_str());
}

char* manager_getLibraryTypeName(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return strdup(man->GetLibraryTypeName(homeId).c_str());
}

int32_t manager_getSendQueueCount(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->GetSendQueueCount(homeId);
}

void manager_logDriverStatistics(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	man->LogDriverStatistics(homeId);
}

//TODO driver_controllerInterface_t manager_getControllerInterfaceType(manager_t m, uint32_t homeId)
// {
// 	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
// 	return man->GetControllerInterfaceType(homeId);
// }

char* manager_getControllerPath(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return strdup(man->GetControllerPath(homeId).c_str());
}

//
// Polling Z-Wave devices.
//

int32_t manager_getPollInterval(manager_t m)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->GetPollInterval();
}

void manager_setPollInterval(manager_t m, int32_t milliseconds, bool intervalBetweenPolls)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	man->SetPollInterval(milliseconds, intervalBetweenPolls);
}

bool manager_enablePoll(manager_t m, valueid_t valueid, uint8_t intensity)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	return man->EnablePoll(*val, intensity);
}

bool manager_disablePoll(manager_t m, valueid_t valueid)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	return man->DisablePoll(*val);
}

bool manager_isPolled(manager_t m, valueid_t valueid)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	return man->isPolled(*val);
}

void manager_setPollIntensity(manager_t m, valueid_t valueid, uint8_t intensity)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	man->SetPollIntensity(*val, intensity);
}

uint8_t manager_getPollIntensity(manager_t m, valueid_t valueid)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	return man->DisablePoll(*val);
}

//
// Node information.
//

struct BoolResult manager_refreshNodeInfo(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->RefreshNodeInfo(homeId, nodeId);
    });
}

struct BoolResult manager_requestNodeState(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->RequestNodeState(homeId, nodeId);
    });
}

struct BoolResult manager_requestNodeDynamic(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->RequestNodeDynamic(homeId, nodeId);
    });
}

struct BoolResult manager_isNodeListeningDevice(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeListeningDevice(homeId, nodeId);
    });
}

struct BoolResult manager_isNodeFrequentListeningDevice(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeFrequentListeningDevice(homeId, nodeId);
    });
}

struct BoolResult manager_isNodeBeamingDevice(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeBeamingDevice(homeId, nodeId);
    });
}

struct BoolResult manager_isNodeRoutingDevice(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeRoutingDevice(homeId, nodeId);
    });
}

struct BoolResult manager_isNodeSecurityDevice(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeSecurityDevice(homeId, nodeId);
    });
}

struct Uint32Result manager_getNodeMaxBaudRate(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<Uint32Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->GetNodeMaxBaudRate(homeId, nodeId);
	});
}

struct Uint8Result manager_getNodeVersion(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<Uint8Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->GetNodeVersion(homeId, nodeId);
	});
}

struct Uint8Result manager_getNodeSecurity(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<Uint8Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->GetNodeSecurity(homeId, nodeId);
	});
}

struct BoolResult manager_isNodeZWavePlus(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeZWavePlus(homeId, nodeId);
	});
}

struct Uint8Result manager_getNodeBasic(manager_t m, uint32_t homeId, uint8_t nodeId)
{
	return exception_boundary_handle<Uint8Result>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->GetNodeBasic(homeId, nodeId);
    });
}

struct Uint8Result manager_getNodeGeneric(manager_t m, uint32_t homeId, uint8_t nodeId)
{
	return exception_boundary_handle<Uint8Result>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->GetNodeGeneric(homeId, nodeId);
    });
}

struct Uint8Result manager_getNodeSpecific(manager_t m, uint32_t homeId, uint8_t nodeId)
{
	return exception_boundary_handle<Uint8Result>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->GetNodeSpecific(homeId, nodeId);
    });
}

struct StrResult manager_getNodeType(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return strdup(man->GetNodeType(homeId, nodeId).c_str());
	});
}

struct StrResult manager_getNodeManufacturerName(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeManufacturerName(homeId, nodeId).c_str());
    });
}

struct StrResult manager_getNodeProductName(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeProductName(homeId, nodeId).c_str());
    });
}

struct StrResult manager_getNodeName(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeName(homeId, nodeId).c_str());
    });
}

struct StrResult manager_getNodeLocation(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeLocation(homeId, nodeId).c_str());
    });
}

struct StrResult manager_getNodeManufacturerId(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeManufacturerId(homeId, nodeId).c_str());
    });
}

struct StrResult manager_getNodeProductType(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeProductType(homeId, nodeId).c_str());
    });
}

struct StrResult manager_getNodeProductId(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeProductId(homeId, nodeId).c_str());
    });
}

struct VoidResult manager_setNodeManufacturerName(manager_t m, uint32_t homeId, uint8_t nodeId, const char* manufacturerName)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        std::string str(manufacturerName);
        man->SetNodeManufacturerName(homeId, nodeId, str);
	});
}

struct VoidResult manager_setNodeProductName(manager_t m, uint32_t homeId, uint8_t nodeId, const char* productName)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        std::string str(productName);
        man->SetNodeProductName(homeId, nodeId, str);
	});
}

struct VoidResult manager_setNodeName(manager_t m, uint32_t homeId, uint8_t nodeId, const char* nodeName)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        std::string str(nodeName);
        man->SetNodeName(homeId, nodeId, str);
	});
}

struct VoidResult manager_setNodeLocation(manager_t m, uint32_t homeId, uint8_t nodeId, const char* location)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        std::string str(location);
        man->SetNodeLocation(homeId, nodeId, str);
    });
}

struct VoidResult manager_setNodeOn(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_void([&] {
        /*OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        man->SetNodeOn(homeId, nodeId);*/
	});
}

struct VoidResult manager_setNodeOff(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_void([&] {
        /*OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        man->SetNodeOff(homeId, nodeId);*/
	});
}

struct VoidResult manager_setNodeLevel(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t level)
{
    return exception_boundary_handle_void([&] {
        /*OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        man->SetNodeLevel(homeId, nodeId, level);*/
	});
}

struct BoolResult manager_isNodeInfoReceived(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->IsNodeInfoReceived(homeId, nodeId);
	});
}

struct BoolResult manager_getNodeClassInformation(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t commandClassId, char **o_name, uint8_t *o_version)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        bool result;
        std::string str;
        result = man->GetNodeClassInformation(homeId, nodeId, commandClassId, &str, o_version);
        if (*o_name) {
            free(*o_name);
        }
        *o_name = strdup(str.c_str());
        return result;
	});
}

struct BoolResult manager_isNodeAwake(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeAwake(homeId, nodeId);
    });
}

struct BoolResult manager_isNodeFailed(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->IsNodeFailed(homeId, nodeId);
    });
}

struct StrResult manager_getNodeQueryStage(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return strdup(man->GetNodeQueryStage(homeId, nodeId).c_str());
	});
}

struct Uint16Result manager_getNodeDeviceType(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<Uint16Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->GetNodeDeviceType(homeId, nodeId);
	});
}

struct StrResult manager_getNodeDeviceTypeString(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return strdup(man->GetNodeDeviceTypeString(homeId, nodeId).c_str());
	});
}

struct Uint8Result manager_getNodeRole(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<Uint8Result>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->GetNodeRole(homeId, nodeId);
	});
}

struct StrResult manager_getNodeRoleString(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodeRoleString(homeId, nodeId).c_str());
    });
}

struct Uint8Result manager_getNodePlusType(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<Uint8Result>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->GetNodePlusType(homeId, nodeId);
	});
}

struct StrResult manager_getNodePlusTypeString(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return strdup(man->GetNodePlusTypeString(homeId, nodeId).c_str());
    });
}

//
// Values.
//

struct StrResult manager_getValueLabel(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return strdup(man->GetValueLabel(*val).c_str());
	});
}

struct VoidResult manager_setValueLabel(manager_t m, valueid_t valueid, const char* value)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::string str(value);
        man->SetValueLabel(*val, str);
    });
}

struct StrResult manager_getValueUnits(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle<StrResult>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        return strdup(man->GetValueUnits(*val).c_str());
    });
}

struct VoidResult manager_setValueUnits(manager_t m, valueid_t valueid, const char* value)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::string str(value);
        man->SetValueUnits(*val, str);
	});
}

struct StrResult manager_getValueHelp(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle<StrResult>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return strdup(man->GetValueHelp(*val).c_str());
	});
}

struct VoidResult manager_setValueHelp(manager_t m, valueid_t valueid, const char* value)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::string str(value);
        man->SetValueHelp(*val, str);
	});
}

struct int32Result manager_getValueMin(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle<int32Result>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetValueMin(*val);
	});
}

struct int32Result manager_getValueMax(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle<int32Result>([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetValueMax(*val);
	});
}

struct BoolResult manager_isValueReadOnly(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->IsValueReadOnly(*val);
    });
}

struct BoolResult manager_isValueWriteOnly(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->IsValueWriteOnly(*val);
    });
}

struct BoolResult manager_isValueSet(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->IsValueSet(*val);
    });
}

struct BoolResult manager_isValuePolled(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->IsValuePolled(*val);
    });
}

struct BoolResult manager_getValueAsBool(manager_t m, valueid_t valueid, bool *o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetValueAsBool(*val, o_value);
	});
}

struct BoolResult manager_getValueAsByte(manager_t m, valueid_t valueid, uint8_t *o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetValueAsByte(*val, o_value);
    });
}

struct BoolResult manager_getValueAsFloat(manager_t m, valueid_t valueid, float *o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetValueAsFloat(*val, o_value);
    });
}

struct BoolResult manager_getValueAsInt(manager_t m, valueid_t valueid, int32_t *o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        return man->GetValueAsInt(*val, o_value);
    });
}

struct BoolResult manager_getValueAsShort(manager_t m, valueid_t valueid, int16_t *o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        return man->GetValueAsShort(*val, o_value);
    });
}

struct BoolResult manager_getValueAsString(manager_t m, valueid_t valueid, char **o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::string str;
        bool result = man->GetValueAsString(*val, &str);
        if (*o_value) {
            free(*o_value);
        }
        *o_value = strdup(str.c_str());
        return result;
	});
}

struct BoolResult manager_getValueAsRaw(manager_t m, valueid_t valueid, zwbytes_t *o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        return man->GetValueAsRaw(*val, &(o_value->data), (uint8_t*)&(o_value->size));
    });
}

struct BoolResult manager_getValueListSelectionAsString(manager_t m, valueid_t valueid, char **o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::string str;
        bool result = man->GetValueListSelection(*val, &str);
        if (*o_value) {
            free(*o_value);
        }
        *o_value = strdup(str.c_str());
        return result;
	});
}

struct BoolResult manager_getValueListSelectionAsInt32(manager_t m, valueid_t valueid, int32_t *o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        return man->GetValueListSelection(*val, o_value);
	});
}

struct BoolResult manager_getValueListItems(manager_t m, valueid_t valueid, zwlist_t **o_value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::vector<std::string> list;
        bool result = man->GetValueListItems(*val, &list);
        if (*o_value) {
            zwlist_free(*o_value);
        }
        *o_value = zwlist_copy(list);
        return result;
	});
}

struct BoolResult manager_getValueFloatPrecision(manager_t m, valueid_t valueid, uint8_t *o_value)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetValueFloatPrecision(*val, o_value);
    });
}

struct BoolResult manager_setValueBool(manager_t m, valueid_t valueid, bool value)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->SetValue(*val, value);
    });
}

struct BoolResult manager_setValueUint8(manager_t m, valueid_t valueid, uint8_t value)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->SetValue(*val, value);
    });
}

struct BoolResult manager_setValueFloat(manager_t m, valueid_t valueid, float value)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->SetValue(*val, value);
    });
}

struct BoolResult manager_setValueInt32(manager_t m, valueid_t valueid, int32_t value)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->SetValue(*val, value);
    });
}

struct BoolResult manager_setValueInt16(manager_t m, valueid_t valueid, int16_t value)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->SetValue(*val, value);
    });
}

struct BoolResult manager_setValueBytes(manager_t m, valueid_t valueid, zwbytes_t *value)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->SetValue(*val, value->data, (uint8_t)value->size);
    });
}

struct BoolResult manager_setValueString(manager_t m, valueid_t valueid, const char* value)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::string str(value);
        return man->SetValue(*val, str);
	});
}

struct BoolResult manager_setValueListSelection(manager_t m, valueid_t valueid, const char* selectedItem)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        std::string str(selectedItem);
        return man->SetValueListSelection(*val, str);
	});
}

struct BoolResult manager_refreshValue(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->RefreshValue(*val);
    });
}

struct VoidResult manager_setChangeVerified(manager_t m, valueid_t valueid, bool verify)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        man->SetChangeVerified(*val, verify);
	});
}

struct BoolResult manager_getChangeVerified(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetChangeVerified(*val);
    });
}

struct BoolResult manager_pressButton(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->PressButton(*val);
    });
}

struct BoolResult manager_releaseButton(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->ReleaseButton(*val);
    });
}

//
// Climate control schedules.
//

struct Uint8Result manager_getNumSwitchPoints(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle<Uint8Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        return man->GetNumSwitchPoints(*val);
	});
}

struct BoolResult manager_setSwitchPoint(manager_t m, valueid_t valueid, uint8_t hours, uint8_t minutes, int8_t setback)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->SetSwitchPoint(*val, hours, minutes, setback);
    });
}

struct BoolResult manager_removeSwitchPoint(manager_t m, valueid_t valueid, uint8_t hours, uint8_t minutes)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->RemoveSwitchPoint(*val, hours, minutes);
    });
}

struct VoidResult manager_clearSwitchPoints(manager_t m, valueid_t valueid)
{
    return exception_boundary_handle_void([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
        return man->ClearSwitchPoints(*val);
	});
}

struct BoolResult manager_getSwitchPoint(manager_t m, valueid_t valueid, uint8_t idx, uint8_t *o_hours, uint8_t *o_minutes, int8_t *o_setback)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    OpenZWave::ValueID *val = (OpenZWave::ValueID*)valueid;
	    return man->GetSwitchPoint(*val, idx, o_hours, o_minutes, o_setback);
    });
}

//
// Configuration parameters.
//

struct BoolResult manager_setConfigParam(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t param, int32_t value, uint8_t size)
{
    return exception_boundary_handle_bool([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->SetConfigParam(homeId, nodeId, param, value, size);
	});
}

struct VoidResult manager_requestConfigParam(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t param)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->RequestConfigParam(homeId, nodeId, param);
    });
}

struct VoidResult manager_requestAllConfigParams(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->RequestAllConfigParams(homeId, nodeId);
    });
}

//
// Groups.
//

struct Uint8Result manager_getNumGroups(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle<Uint8Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->GetNumGroups(homeId, nodeId);
	});
}

struct Uint32Result manager_getAssociations(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, uint8_t **o_associations)
{
	return exception_boundary_handle<Uint32Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->GetAssociations(homeId, nodeId, groupIdx, o_associations);
	});
}

//TODO uint32_t manager_getAssociations(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, instanceassociation_t **o_associations)
// {
// 	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
// 	return man->GetAssociations(homeId, nodeId, groupIdx, ...);
// }

struct Uint8Result manager_getMaxAssociations(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx)
{
    return exception_boundary_handle<Uint8Result>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return man->GetMaxAssociations(homeId, nodeId, groupIdx);
	});
}

struct StrResult manager_getGroupLabel(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx)
{
    return exception_boundary_handle<StrResult>([&] {
        OpenZWave::Manager *man = (OpenZWave::Manager*)m;
        return strdup(man->GetGroupLabel(homeId, nodeId, groupIdx).c_str());
    });
}

struct VoidResult manager_addAssociation(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, uint8_t targetNodeId, uint8_t instance)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->AddAssociation(homeId, nodeId, groupIdx, targetNodeId, instance);
    });
}

struct VoidResult manager_removeAssociation(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t groupIdx, uint8_t targetNodeId, uint8_t instance)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->RemoveAssociation(homeId, nodeId, groupIdx, targetNodeId, instance);
    });
}

//
// Notifications.
//

static void manager_notificationHandler(OpenZWave::Notification const* notification, void* userdata)
{
	// Note that OpenZWave will delete the notification object when it thinks we
	// are done with it. Probably when we return control to it.
	notification_t noti = (notification_t)notification;
	goNotificationCB(noti, userdata);
}

bool manager_addWatcher(manager_t m, void *userdata)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->AddWatcher(manager_notificationHandler, userdata);
}

bool manager_removeWatcher(manager_t m, void *userdata)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->RemoveWatcher(manager_notificationHandler, userdata);
}

//
// Controller commands.
//

void manager_resetController(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	man->ResetController(homeId);
}

void manager_softReset(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	man->SoftReset(homeId);
}

bool manager_cancelControllerCommand(manager_t m, uint32_t homeId)
{
	OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	return man->CancelControllerCommand(homeId);
}

//
// Network commands.
//

struct VoidResult manager_testNetworkNode(manager_t m, uint32_t homeId, uint8_t nodeId, uint32_t count)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->TestNetworkNode(homeId, nodeId, count);
    });
}

struct VoidResult manager_testNetwork(manager_t m, uint32_t homeId, uint32_t count)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->TestNetwork(homeId, count);
    });
}

struct VoidResult manager_healNetworkNode(manager_t m, uint32_t homeId, uint8_t nodeId, bool doRR)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->HealNetworkNode(homeId, nodeId, doRR);
    });
}

struct VoidResult manager_healNetwork(manager_t m, uint32_t homeId, bool doRR)
{
    return exception_boundary_handle_void([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    man->HealNetwork(homeId, doRR);
    });
}

struct BoolResult manager_addNode(manager_t m, uint32_t homeId, bool doSecurity)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->AddNode(homeId, doSecurity);
    });
}

struct BoolResult manager_removeNode(manager_t m, uint32_t homeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->RemoveNode(homeId);
    });
}

struct BoolResult manager_removeFailedNode(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->RemoveFailedNode(homeId, nodeId);
    });
}

struct BoolResult manager_hasNodeFailed(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->HasNodeFailed(homeId, nodeId);
    });
}

struct BoolResult manager_requestNodeNeighborUpdate(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->RequestNodeNeighborUpdate(homeId, nodeId);
    });
}

struct BoolResult manager_assignReturnRoute(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->AssignReturnRoute(homeId, nodeId);
    });
}

struct BoolResult manager_deleteAllReturnRoutes(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->DeleteAllReturnRoutes(homeId, nodeId);
    });
}

struct BoolResult manager_sendNodeInformation(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->SendNodeInformation(homeId, nodeId);
    });
}

struct BoolResult manager_createNewPrimary(manager_t m, uint32_t homeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->CreateNewPrimary(homeId);
    });
}

struct BoolResult manager_receiveConfiguration(manager_t m, uint32_t homeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->ReceiveConfiguration(homeId);
    });
}

struct BoolResult manager_replaceFailedNode(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->ReplaceFailedNode(homeId, nodeId);
    });
}

struct BoolResult manager_transferPrimaryRole(manager_t m, uint32_t homeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->TransferPrimaryRole(homeId);
    });
}

struct BoolResult manager_requestNetworkUpdate(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->RequestNetworkUpdate(homeId, nodeId);
    });
}

struct BoolResult manager_replicationSend(manager_t m, uint32_t homeId, uint8_t nodeId)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->ReplicationSend(homeId, nodeId);
    });
}

struct BoolResult manager_createButton(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t buttonid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->CreateButton(homeId, nodeId, buttonid);
    });
}

struct BoolResult manager_deleteButton(manager_t m, uint32_t homeId, uint8_t nodeId, uint8_t buttonid)
{
    return exception_boundary_handle_bool([&] {
	    OpenZWave::Manager *man = (OpenZWave::Manager*)m;
	    return man->DeleteButton(homeId, nodeId, buttonid);
    });
}
