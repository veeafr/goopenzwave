package goopenzwave

type Uint8HomeNodeFunc = func(uint32, uint8)(uint8, error)
type UStrHomeNodeFunc = func(uint32, uint8)(string, error)

func withDefaultUint8HN(fun Uint8HomeNodeFunc, homeID uint32, nodeID uint8) uint8 {
	val, err := fun(homeID, nodeID)
	if err != nil {
		return 0
	} else {
		return val
	}
}

func withDefaultStringHN(fun UStrHomeNodeFunc, homeID uint32, nodeID uint8) string {
	val, err := fun(homeID, nodeID)
	if err != nil {
		return ""
	} else {
		return val
	}
}

func withDefaultString(fun func()(string, error)) string {
	val, err := fun()
	if err != nil {
		return ""
	} else {
		return val
	}
}

func withDefaultInt32(fun func()(int32, error)) int32 {
	val, err := fun()
	if err != nil {
		return 0
	} else {
		return val
	}
}

func withDefaultBool(fun func()(bool, error)) bool {
	val, err := fun()
	if err != nil {
		return false
	} else {
		return val
	}
}

